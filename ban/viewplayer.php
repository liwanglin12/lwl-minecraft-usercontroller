﻿<?php
/*  BanManagement ?2012, a web interface for the Bukkit plugin BanManager
    by James Mortemore of http://www.frostcast.net
	is licenced under a Creative Commons
	Attribution-NonCommercial-ShareAlike 2.0 UK: England & Wales.
	Permissions beyond the scope of this licence 
	may be available at http://creativecommons.org/licenses/by-nc-sa/2.0/uk/.
	Additional licence terms at https://raw.github.com/confuser/Ban-Management/master/banmanagement/licence.txt
*/
if(!isset($_GET['server']) || !is_numeric($_GET['server']))
	redirect('index.php');
else if(!isset($settings['servers'][$_GET['server']]))
	redirect('index.php');
else if(!isset($_GET['player']) || empty($_GET['player']))
	redirect('index.php');
else if(isset($_GET['player']) && preg_match('/[^a-z0-9_]{2,16}/i', $_GET['player']))
	redirect('index.php');
else {
	// Get the server details
	$server = $settings['servers'][$_GET['server']];

	// Clear old players
	clearCache($_GET['server'].'/players', 300);
	clearCache($_GET['server'].'/mysqlTime', 300);
	
	// Check if they are logged in as an admin
	if(isset($_SESSION['admin']) && $_SESSION['admin'])
		$admin = true;
	else
		$admin = false;
	
	// Check if the player exists
	$currentBans = cache("SELECT * FROM ".$server['bansTable']." WHERE banned = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);
	$pastBans = cache("SELECT * FROM ".$server['recordTable']." WHERE banned = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);
	$currentMutes = cache("SELECT * FROM ".$server['mutesTable']." WHERE muted = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);
	$pastMutes = cache("SELECT * FROM ".$server['mutesRecordTable']." WHERE muted = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);
	$pastKicks = cache("SELECT * FROM ".$server['kicksTable']." WHERE kicked = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);
	$pastWarnings = cache("SELECT * FROM ".$server['warningsTable']." WHERE warned = '".$_GET['player']."'", 300, $_GET['server'].'/players', $server);

	if(count($currentBans) == 0 && count($pastBans) == 0 && count($currentMutes) == 0 && count($pastMutes) == 0 && count($pastKicks) == 0 && $pastWarnings == 0) {
		errors('Player does not exist');
		?><a href="index.php" class="btn btn-primary">New Search</a><?php
	} else {
		// They have been banned, naughty!
		// Now check the time differences!
		$timeDiff = cache('SELECT ('.time().' - UNIX_TIMESTAMP(now()))/3600 AS mysqlTime', 5, $_GET['server'].'/mysqlTime', $server); // Cache it for a few seconds
		
		$mysqlTime = $timeDiff['mysqlTime'];
		$mysqlTime = ($mysqlTime > 0)  ? floor($mysqlTime) : ceil ($mysqlTime);
		$mysqlSecs = ($mysqlTime * 60) * 60;
		?>
		<div class="row">
			<div class="col-lg-3">
					<div class="player_information">
							<span class="skin" data-minecraft-username="<?php echo $_GET['player'];?>"></span>
							<span id="player_name"><?php echo $_GET['player'];?></span>
					</div>
			</div>
			<div class="col-lg-9" id="player_ban_info">
					<h4>服务器: <?php echo $server['name']; ?></h4>
		<?php
		$id = array_keys($settings['servers']);
		$i = 0;
		$html = '';
		if(count($settings['servers']) > 1) {
			echo '
			<p>Change Server: ';
			foreach($settings['servers'] as $serv) {
				if($serv['name'] != $server['name']) {
					$html .= '<a href="index.php?action=viewplayer&player='.$_GET['player'].'&server='.$id[$i].'">'.$serv['name'].'</a>, ';
				}
				++$i;
			}
			echo substr($html, 0, -2).'
			</p>';
		}
		
		if((isset($settings['player_current_ban']) && $settings['player_current_ban']) || !isset($settings['player_current_ban'])) {
			?>
			<br />
			<table id="current-ban" class="table table-striped table-bordered">
				<caption>当前的封禁</caption>
				<tbody>
				<?php
			if(count($currentBans) == 0) {
				echo '
					<tr>
						<td colspan="2">无</td>
					</tr>';
			} else {
				$reason = str_replace(array('&quot;', '"'), array('&#039;', '\''), $currentBans['ban_reason']);
				echo '
					<tr>
						<td>时长:</td>
						<td class="expires">';
				if($currentBans['ban_expires_on'] == 0)
					echo '<span class="label label-danger">永久</span>';
				else {
					$currentBans['ban_expires_on'] = $currentBans['ban_expires_on'] + $mysqlSecs;
					$currentBans['ban_time'] = $currentBans['ban_time'] + $mysqlSecs;
					$expires = $currentBans['ban_expires_on'] - time();
					
					if($expires > 0)
						echo '<time datetime="'.date('c', $currentBans['ban_expires_on']).'">'.secs_to_h($expires).'</time>';
					else
						echo '现在';
				}
				
				echo '</td>
					</tr>
					<tr>
						<td>操作者:</td>
						<td>'.$currentBans['banned_by'].'</td>
					</tr>
					<tr>
						<td>操作时间:</td>
						<td>'.date('Y/m/d h:i:s A', $currentBans['ban_time']).'</td>
					</tr>
					<tr>
						<td>原因:</td>
						<td class="reason">'.$currentBans['ban_reason'].'</td>
					</tr>';
				if(!empty($currentBans['server'])) {
					echo '
					<tr>
						<td>服务器:</td>
						<td>'.$currentBans['server'].'</td>
					</tr>';
				}
			}
				?>
				</tbody><?php
			if($admin && count($currentBans) != 0) {
				echo '
				<tfoot>
					<tr>
						<td colspan="2">
							<a class="btn btn-warning edit" title="Edit" href="#editban" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span> 修改</a>
							<a class="btn btn-danger delete" title="Unban" data-role="confirm" href="index.php?action=deleteban&ajax=true&authid='.sha1($settings['password']).'&server='.$_GET['server'].'&id='.$currentBans['ban_id'].'" data-confirm-title="Unban '.$_GET['player'].'" data-confirm-body="你确定要解除封禁吗？ '.$_GET['player'].'?<br />该操作无法恢复！"><span class="glyphicon glyphicon-trash"></span> 解除</a>
						</td>
					</tr>
				</tfoot>';
			}
				?>
			</table>
			<?php
			if(isset($settings['player_current_ban_extra_html'])) {
				$extra = htmlspecialchars_decode($settings['player_current_ban_extra_html'], ENT_QUOTES);
				$extra = str_replace(array('{SERVER}', '{SERVERID}', '{NAME}'), array($server['name'], $_GET['server'], $_GET['player']), $extra);
				
				echo '
			<div id="current-ban-extra">
				'.$extra.'
			</div>';
			}
			if($admin && count($currentBans) != 0) {?>
			<div class="modal fade" id="editban">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form-horizontal" action="" method="post">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>修改</h3>
							</div>
							<div class="modal-body">
								<div class="control-group">
									<label class="control-label" for="yourtime">你的时间:</label>
									<div class="controls">
										<span class="yourtime"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="servertime">服务器时间:</label>
									<div class="controls">
										<span class="servertime"><?php echo date('Y/m/d H:i:s', time() + $mysqlSecs); ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="bandatetime">处罚到期的服务器时间:</label>
									<div class="controls">
										<div class="input-group date datetimepicker">
										<?php
											echo '						
											<span class="input-group-btn">
												<button class="btn btn-danger bantype" type="button">';
											if($currentBans['ban_expires_on'] == 0)
												echo '永久';
											else
												echo '暂时';
										
											echo '</button>
											</span>
											<input type="text" class="form-control required';
										
											if($currentBans['ban_expires_on'] == 0)
												echo ' disabled" disabled="disabled"';
											else
												echo '"'; 
										
											echo ' name="expires" data-format="YYYY/MM/DD hh:mm:ss" value="';

											if($currentBans['ban_expires_on'] == 0)
												echo '';
											else
												echo date('y/m/d H:i:s', $currentBans['ban_expires_on']);
											
											echo '" id="bandatetime" />';
										?>
											<span class="input-group-addon">
												<i class="glyphicon glyphicon-calendar"></i>
											</span>
										</div>
									</div>
								</div>
								<label for="banreason">原因:</label>
								<textarea id="banreason" name="reason" class="form-control" rows="4"><?php echo $currentBans['ban_reason']; ?></textarea>
							</div>
							<div class="modal-footer">
								<a href="#" class="btn" data-dismiss="modal">关闭</a>
								<input type="submit" class="btn btn-primary" value="保存" />
							</div>
							<input type="hidden" name="id" value="<?php echo $currentBans['ban_id']; ?>" />
							<input type="hidden" name="server" value="<?php echo $_GET['server']; ?>" />
							<input type="hidden" name="expiresTimestamp" value="" />
						</form>
					</div>
				</div>
			</div>
			<?php
			}
		}
		
		if((isset($settings['player_current_mute']) && $settings['player_current_mute']) || !isset($settings['player_current_mute'])) {
			?>
			<br />
			<table id="current-mute" class="table table-striped table-bordered">
				<caption>禁言</caption>
				<tbody>
				<?php
			if(count($currentMutes) == 0) {
				echo '
					<tr>
						<td colspan="2">无</td>
					</tr>';
			} else {
				$reason = str_replace(array('&quot;', '"'), array('&#039;', '\''), $currentMutes['mute_reason']);
				echo '
					<tr>
						<td>到期时间:</td>
						<td class="expires">';
				if($currentMutes['mute_expires_on'] == 0)
					echo '<span class="label label-danger">永久</span>';
				else {
					$currentMutes['mute_expires_on'] = $currentMutes['mute_expires_on'] + $mysqlSecs;
					$currentMutes['mute_time'] = $currentMutes['mute_time'] + $mysqlSecs;
					$expires = $currentMutes['mute_expires_on'] - time();
				
					if($expires > 0)
						echo '<time datetime="'.date('c', $currentMutes['mute_expires_on']).'">'.secs_to_h($expires).'</time>';
					else
						echo '现在';
				}
				echo '</td>
					</tr>
					<tr>
						<td>操作者:</td>
						<td>'.$currentMutes['muted_by'].'</td>
					</tr>
					<tr>
						<td>操作时间:</td>
						<td>'.date('jS F Y h:i:s A', $currentMutes['mute_time']).'</td>
					</tr>
					<tr>
						<td>原因:</td>
						<td class="reason">'.$currentMutes['mute_reason'].'</td>
					</tr>
					<tr>';
				if(!empty($currentMutes['server'])) {
					echo '
					<tr>
						<td>服务器:</td>
						<td>'.$currentMutes['server'].'</td>
					</tr>';
				}
			}
				?>
				
				</tbody>
				<?php
			if($admin && count($currentMutes) != 0) {
				echo '
				<tfoot>
					<tr>
						<td colspan="2">
							<a class="btn btn-warning edit" title="Edit" href="#editmute" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span> 修改</a>
							<a class="btn btn-danger delete" title="Unban" data-role="confirm" href="index.php?action=deletemute&ajax=true&authid='.sha1($settings['password']).'&server='.$_GET['server'].'&id='.$currentMutes['mute_id'].'" data-confirm-title="Unban '.$_GET['player'].'" data-confirm-body="你确定要解除禁言吗？ '.$_GET['player'].'?<br />该操作无法恢复！"><span class="glyphicon glyphicon-trash"></span> 解除禁言</a>
						</td>
					</tr>
				</tfoot>';
			}
				?>
			
			</table><?php
			if(isset($settings['player_current_mute_extra_html'])) {
				$extra = htmlspecialchars_decode($settings['player_current_mute_extra_html'], ENT_QUOTES);
				$extra = str_replace(array('{SERVER}', '{SERVERID}', '{NAME}'), array($server['name'], $_GET['server'], $_GET['player']), $extra);
				
				echo '
			<div id="current-mute-extra">
				'.$extra.'
			</div>';
			}
			if($admin && count($currentMutes) != 0) {?>
			
			<div class="modal fade" id="editmute">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form-horizontal" action="" method="post">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h3>编辑</h3>
							</div>
							<div class="modal-body">
								<div class="container">
									<div class="form-group">
										<label class="control-label" for="yourtime">你的时间:</label>
											<span class="yourtime"></span>
									</div>
									<div class="form-group">
										<label class="control-label" for="servertime">服务器时间:</label>
											<span class="servertime"><?php echo date('Y/m/d H:i:s', time() + $mysqlSecs); ?></span>
									</div>
								<!--	<div class="form-group">
										<label class="control-label" for="mutedatetime">Expires Server Time:</label>
										<div class="input-group">
											<div class="input datetimepicker date">
											<?php
								/*
												echo '						
												<div class="input-group-addon">
													<button class="btn btn-danger bantype" type="button">';
											if($currentMutes['mute_expires_on'] == 0)
												echo '永久';
											else
												echo '临时';
										
											echo '</button>
													<input type="text" class="required';
					
											if($currentMutes['mute_expires_on'] == 0)
												echo ' disabled" disabled="disabled"';
											else
												echo '"'; 
										
											echo ' name="expires" data-format="YY/MM/DDyy hh:mm:ss" value="';

											if($currentMutes['mute_expires_on'] == 0)
												echo '';
											else
												echo date('y/m/d H:i:s', $currentMutes['mute_expires_on']);
											
											echo '" id="mutedatetime" />';

								*/			?>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
												</div>
											</div>
										</div>
									</div> -->
									<div class="form-group">
										<label class="control-label" for="mutereason">原因:</label>
											<textarea id="mutereason" class="form-control" name="reason" rows="4"><?php echo $currentMutes['mute_reason']; ?></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<a href="#" class="btn" data-dismiss="modal">关闭</a>
								<input type="submit" class="btn btn-primary" value="保存" />
							</div>
							<input type="hidden" name="id" value="<?php echo $currentMutes['mute_id']; ?>" />
							<input type="hidden" name="server" value="<?php echo $_GET['server']; ?>" />
							<input type="hidden" name="expiresTimestamp" value="" />
						</form>
					</div>
				</div>
			</div><?php
			}
		}
		
		if((isset($settings['player_previous_bans']) && $settings['player_previous_bans']) || !isset($settings['player_previous_bans'])) {
		?>
			<br />
			<table class="table table-striped table-bordered" id="previous-bans">
				<caption>历史封禁记录</caption>
				<thead>
					<tr>
						<th>用户</th>
						<th>原因</th>
						<th>操作者</th>
						<th>封禁时间</th>
						<th>长度</th>
						<th>解除者</th>
						<th>解封时间</th><?php
			if(!isset($pastBans[0]) || (isset($pastBans[0]) && !is_array($pastBans[0])))
				$pastBans = array($pastBans);
			$serverName = false;
			foreach($pastBans as $r) {
				if(!empty($r['server'])) {
					$serverName = true;
					break;
				}
			}
			if($serverName) {
				echo '
						<th>服务器</th>';
			}
			if($admin)
				echo '
						<th></th>';
				?>
				
					</tr>
				</thead>
				<tbody><?php
			if(isset($pastBans[0]) && count($pastBans[0]) == 0) {
				echo '
					<tr>
						<td colspan="8">无</td>
					</tr>';
			} else {
				$i = 1;
				foreach($pastBans as $r) {
					$r['ban_reason'] = str_replace(array('&quot;', '"'), array('&#039;', '\''), $r['ban_reason']);
					$r['ban_expired_on'] = ($r['ban_expired_on'] != 0 ? $r['ban_expired_on'] + $mysqlSecs : $r['ban_expired_on']);
					$r['ban_time'] = $r['ban_time'] + $mysqlSecs;
					$r['unbanned_time'] = $r['unbanned_time'] + $mysqlSecs;

					echo '
					<tr>
						<td>'.$i.'</td>
						<td>'.$r['ban_reason'].'</td>
						<td>'.$r['banned_by'].'</td>
						<td>'.date('y-m-d H:i:s ', $r['ban_time']).'</td>
						<td>'.($r['ban_expired_on'] == 0 ? '永久' : secs_to_h($r['ban_expired_on'] - $r['ban_time'])).'</td>
						<td>'.$r['unbanned_by'].'</td>
						<td>'.date('y-m-d H:i:s ', $r['unbanned_time']).'</td>'.($serverName ? '
						<td>'.$r['server'].'</td>' : '').($admin ? '
						<td class="admin-options"><a href="#" class="btn btn-danger delete" title="移除" data-server="'.$_GET['server'].'" data-record-id="'.$r['ban_record_id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>' : '').'
					</tr>';
					++$i;
				}
			}
				?>
				
				</tbody>
			</table><?php
		} 
		
		if((isset($settings['player_previous_mutes']) && $settings['player_previous_mutes']) || !isset($settings['player_previous_mutes'])) {
		?>
			<br />
			<table class="table table-striped table-bordered" id="previous-mutes">
				<caption>历史禁言记录</caption>
				<thead>
					<tr>
						<th>用户</th>
						<th>原因</th>
						<th>操作者</th>
						<th>禁言时间</th>
						<th>长度</th>
						<th>解除者</th>
						<th>解禁时间</th><?php
			if(isset($pastMutes[0]) && !is_array($pastMutes[0]))
				$pastMutes = array($pastMutes);
			$serverName = false;
			foreach($pastMutes as $r) {
				if(!empty($r['server'])) {
					$serverName = true;
					break;
				}
			}
			if($serverName) {
					echo '
						<th>服务器</th>';
			}
			if($admin)
				echo '
						<th></th>';
				?>
					
					</tr>
				</thead>
				<tbody><?php
			if(count($pastMutes) == 0) {
				echo '
					<tr>
						<td colspan="8">无</td>
					</tr>';
			} else {
				$i = 1;
				foreach($pastMutes as $r) {
					$r['mute_reason'] = str_replace(array('&quot;', '"'), array('&#039;', '\''), $r['mute_reason']);
					$r['mute_expired_on'] = ($r['mute_expired_on'] != 0 ? $r['mute_expired_on'] + $mysqlSecs : $r['mute_expired_on']);
					$r['mute_time'] = $r['mute_time'] + $mysqlSecs;
					echo '
					<tr>
						<td>'.$i.'</td>
						<td>'.$r['mute_reason'].'</td>
						<td>'.$r['muted_by'].'</td>
						<td>'.date('y/m/d', $r['mute_time']).'</td>
						<td>'.($r['mute_expired_on'] == 0 ? '永久' : secs_to_h($r['mute_expired_on'] - $r['mute_time'])).'</td>
						<td>'.$r['unmuted_by'].'</td>
						<td>'.date('y/m/d', $r['unmuted_time']).'</td>'.($serverName ? '
						<td>'.$r['server'].'</td>' : '').($admin ? '
						<td class="admin-options"><a href="#" class="btn btn-danger delete" title="移除" data-server="'.$_GET['server'].'" data-record-id="'.$r['mute_record_id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>' : '').'
					</tr>';
					++$i;
				}
			}
				?>
				
				</tbody>
			</table><?php
		}
		
		if((isset($settings['player_warnings']) && $settings['player_warnings']) || !isset($settings['player_warnings'])) {
			?>
			<br />
			<table class="table table-striped table-bordered" id="previous-warnings">
				<caption>历史警告记录</caption>
				<thead>
					<tr>
						<th>用户</th>
						<th>原因</th>
						<th>操作者</th>
						<th>警告时间</th><?php
			if(!isset($pastWarnings[0]) || (isset($pastWarnings[0]) && !is_array($pastWarnings[0])))
				$pastWarnings = array($pastWarnings);
			$serverName = false;
			foreach($pastWarnings as $r) {
				if(!empty($r['server'])) {
					$serverName = true;
					break;
				}
			}
			if($serverName) {
				echo '
						<th>服务器</th>';
			}
			if($admin)
				echo '
						<th></th>';
				?>
				
					</tr>
				</thead>
				<tbody><?php
			if(isset($pastWarnings[0]) && count($pastWarnings[0]) == 0) {
				echo '
					<tr>
						<td colspan="8">无</td>
					</tr>';
			} else {
				$i = 1;
				foreach($pastWarnings as $r) {
					$r['warn_reason'] = str_replace(array('&quot;', '"'), array('&#039;', '\''), $r['warn_reason']);
					$r['warn_time'] = $r['warn_time'] + $mysqlSecs;

					echo '
					<tr>
						<td>'.$i.'</td>
						<td>'.$r['warn_reason'].'</td>
						<td>'.$r['warned_by'].'</td>
						<td>'.date('y-m-d H:i:s ', $r['warn_time']).'</td>'.($serverName ? '
						<td>'.$r['server'].'</td>' : '').($admin ? '
						<td class="admin-options"><a href="#" class="btn btn-danger delete" title="移除" data-server="'.$_GET['server'].'" data-record-id="'.$r['warn_id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>' : '').'
					</tr>';
					++$i;
				}
			}
				?>
				
				</tbody>
			</table><?php
		}
		
		if((isset($settings['player_kicks']) && $settings['player_kicks']) || !isset($settings['player_kicks'])) {		
		?>
			<br />
			<table class="table table-striped table-bordered" id="previous-kicks">
				<caption>历史踢出记录</caption>
				<thead>
					<tr>
						<th>用户</th>
						<th>原因</th>
						<th>操作者</th>
						<th>踢出时间</th><?php
			if(isset($pastKicks[0]) && !is_array($pastKicks[0]))
				$pastKicks = array($pastKicks);
			$serverName = false;
			foreach($pastKicks as $r) {
				if(!empty($r['server'])) {
					$serverName = true;
					break;
				}
			}
			if($serverName) {
					echo '
						<th>服务器</th>';
			}
			if($admin)
				echo '
						<th></th>';
				?>
					
					</tr>
				</thead>
				<tbody><?php
			if(count($pastKicks) == 0) {
				echo '
					<tr>
						<td colspan="8">无</td>
					</tr>';
			} else {
				$i = 1;
				foreach($pastKicks as $r) {
					$r['kick_reason'] = str_replace(array('&quot;', '"'), array('&#039;', '\''), $r['kick_reason']);
					$r['kick_time'] = $r['kick_time'] + $mysqlSecs;
					echo '
					<tr>
						<td>'.$i.'</td>
						<td>'.$r['kick_reason'].'</td>
						<td>'.$r['kicked_by'].'</td>
						<td>'.date('y/m/d', $r['kick_time']).'</td>'.($serverName ? '
						<td>'.$r['server'].'</td>' : '').($admin ? '
						<td class="admin-options"><a href="#" class="btn btn-danger delete" title="移除" data-server="'.$_GET['server'].'" data-record-id="'.$r['kick_id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>' : '').'
					</tr>';
					++$i;
				}
			}
				?>
				
				</tbody>
			</table><?php
		} ?>
		</div>
	</div>
		<?php
	}
}
?>