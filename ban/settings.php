<?php
$settings['submit_buttons_after_html'] = '';
$settings['submit_buttons_before_html'] = '';
$settings['iframe_protection'] = true;
/**
* This is the general configuaration file for Ban Management.
* In here you can control your encoding for server transfers,
* define what tables you want enabled, set your password for ACP,
* and more. 
**/

$settings['utf8'] = true;
$settings['latest_bans'] = true;
$settings['latest_mutes'] = true;
$settings['latest_warnings'] = true;
$settings['servers'] = 'a:1:{i:0;a:13:{s:4:"name";s:15:"哲学服务器";s:4:"host";s:14:"localhost:3306";s:8:"database";s:10:"lwlcom_ban";s:8:"username";s:11:"lwlcom_zxmc";s:8:"password";s:12:"ZXMC-sql0426";s:9:"bansTable";s:7:"bm_bans";s:11:"recordTable";s:14:"bm_ban_records";s:7:"ipTable";s:10:"bm_ip_bans";s:13:"ipRecordTable";s:13:"bm_ip_records";s:10:"mutesTable";s:8:"bm_mutes";s:16:"mutesRecordTable";s:16:"bm_mutes_records";s:10:"kicksTable";s:8:"bm_kicks";s:13:"warningsTable";s:11:"bm_warnings";}}';
$settings['password'] = 'ZXMC-bantool0426'; // 管理面板的密码（请设置一个复杂密码）
$settings['footer'] = '© <a href="http://jq.qq.com/?_wv=1027&k=WGVINq">哲学服务器 </a>'.date('Y');
$settings['admin_link'] = true; // 页脚显示管理面板的按钮
$settings['bm_info'] = true; // 显示插件介绍（搜索框右边）
$settings['bm_info_icon'] = true; // 在插件介绍那边显示图标
$settings['pastbans'] = true; // 在搜索框下方显示最近封禁的玩家

$settings['player_current_ban'] = true;
$settings['player_current_mute'] = true;
$settings['player_previous_bans'] = true;
$settings['player_previous_mutes'] = true;
$settings['player_kicks'] = true;
$settings['player_warnings'] = true;
$settings['player_current_ban_extra_html'] = '';
$settings['player_current_mute_extra_html'] = '';
	
/**
* 一些语言信息
**/

$language['brand'] = '封禁管理系统'; // The branding of all pages
$language['header-title'] = '用户状态'; // Edit the 'Account Status' text above the search
$language['description'] = ''; // Meta Description for search engines
$language['title'] = '封禁管理系统'; // Title of all pages
$language['latest_bans_title'] = '最新封禁玩家'; // The text displayed over the latest bans table
$language['latest_mutes_title'] = '最新禁言玩家'; // The text displayed over the latest mutes table
$language['latest_warnings_title'] = '最新警告玩家'; // The text displayed over the latest warnings table
$language['nav-home'] = '首页'; // The text displayed in the navbar for 'Home'
$language['nav-stats'] = '数据统计'; // The text displayed in the navbar for 'Servers'
$language['past_player_bans'] = '封禁记录'; // The text displayed on the homepage
$language['bm_info_text'] = // 搜索框右边显示的信息
' 
通过本系统，你可以方便地查看玩家被处罚的时间、原因。

';

/**
* These are the settings for editing the layout of Ban Management
**/

$theme['navbar-dark'] = false; // 是否使用黑色导航栏

?>