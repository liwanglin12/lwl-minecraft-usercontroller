<?php
/*  BanManagement � 2012, a web interface for the Bukkit plugin BanManager
    by James Mortemore of http://www.frostcast.net
	is licenced under a Creative Commons
	Attribution-NonCommercial-ShareAlike 2.0 UK: England & Wales.
	Permissions beyond the scope of this licence 
	may be available at http://creativecommons.org/licenses/by-nc-sa/2.0/uk/.
	Additional licence terms at https://raw.github.com/confuser/Ban-Management/master/banmanagement/licence.txt
*/
if(empty($settings['password']) || $settings['password'] == 'password')
	errors('你还没有设置密码，安全起见，请先到settings.php设置一个！');
else if(isset($_SESSION['failed_attempts']) && $_SESSION['failed_attempts'] > 4) {
	die(errors('密码错误太多次了。请过半小时再试！'));
	if($_SESSION['failed_attempt'] < time())
		unset($_SESSION['failed_attempts']);
} else if(!isset($_SESSION['admin']) && !isset($_POST['password'])) {
	?><form action="" method="post" class="well form-inline">
	<h3>管理面板 <small>&mdash; 如果你忘记密码，请你到settings.php更改</small></h3>
	<div class="row">
		<div class="col-lg-6">
		<?php
			if(!empty($errors)){
				foreach ($errors as $error) {
					echo $error;
				}
			}
		?>
			<div class="input-group">
			<input type="password" class="form-control" name="password" placeholder="输入密码">
				<span class="input-group-btn">
				<button class="btn btn-info" type="submit">登陆</button>
	    		</span>
	    	</div>
	    </div>
    </div>
    </form><?php
} else if(isset($_POST['password']) && !isset($_SESSION['admin'])) {
	if(htmlspecialchars_decode($_POST['password'], ENT_QUOTES) != $settings['password']) {
		//set how long we want them to have to wait after 5 wrong attempts
		$time = 1800; //make them wait 30 mins
		if(isset($_SESSION['failed_attempts']))
			++$_SESSION['failed_attempts']; 
		else
			$_SESSION['failed_attempts'] = 1;
		$_SESSION['failed_attempt'] = time() + $time;
		redirect('index.php?action=admin');
	} else {
		$_SESSION['admin'] = true;
		redirect('index.php?action=admin');
	}
} else if(isset($_SESSION['admin']) && $_SESSION['admin']) {
	?>
	<table class="table table-striped table-bordered" id="servers">
		<thead>
			<tr>
				<th>服务器名</th>
				<th>功能</th>
			</tr>
		</thead>
		<tbody><?php
	if(empty($settings['servers']))
		echo '<tr id="noservers"><td colspan="2">No Servers Defined</td></tr>';
	else {
		$id = array_keys($settings['servers']);
		$i = 0;
		$count = count($settings['servers']) - 1;
		
		foreach($settings['servers'] as $server) {
			echo '
				<tr>
					<td>'.$server['name'].'</td>
					<td>
						<a href="#" class="btn btn-danger deleteServer" data-serverid="'.$id[$i].'"><span class="glyphicon glyphicon-trash"></span></a>';
			if($count > 0) {
				if($i == 0)
					echo '
					<a href="#" class="btn reorderServer" data-order="down" data-serverid="'.$id[$i].'"><span class="glyphicon glyphicon-arrow-down"></span></a>';
				else if($i == $count)
					echo '
					<a href="#" class="btn reorderServer" data-order="up" data-serverid="'.$id[$i].'"><span class="glyphicon glyphicon-arrow-up"></span></a>';
				else {
					echo '
					<a href="#" class="btn reorderServer" data-order="up" data-serverid="'.$id[$i].'"><span class="glyphicon glyphicon-arrow-up"></span></a>
					<a href="#" class="btn reorderServer" data-order="down" data-serverid="'.$id[$i].'"><span class="glyphicon glyphicon-arrow-down"></span></a>';
				}
			}
			echo '
					</td>
				</tr>';
			++$i;
		}
	}
		?>
		
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
		<?php
	if(!is_writable('settings.php')) {
		echo '<a class="btn btn-primary btn-large disabled" href="#addserver" title="配置文件无法写入">添加服务器</a>';
	} else
		echo '<a class="btn btn-primary btn-large" href="#addserver" data-toggle="modal">添加服务器</a>';
	?>
	
				</td>
			</tr>
		</tfoot>
	</table>
	<div class="modal fade" id="addserver">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" action="" method="post">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3>Add Server</h3>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="form-group">
								<label class="control-label" for="servername">服务器名称:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="servername" id="servername">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="host">MySQL数据库地址:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="host" id="host">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="database">MySQL数据库名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="database" id="database">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="username">MySQL数据库用户名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="username" id="usernme">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="password">MySQL数据库密码:</label>
								<div class="controls">
									<input type="password" class="form-control fixedWidth" name="password" id="password">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="banstable">封禁表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="banstable" id="banstable" value="bm_bans">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="recordtable">封禁记录表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="recordtable" id="recordtable" value="bm_ban_records">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="iptable">封禁IP表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="iptable" id="iptable" value="bm_ip_bans">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="iprecordtable">IP记录表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="iprecordtable" id="iprecordtable" value="bm_ip_records">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="mutestable">禁言表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="mutestable" id="mutestable" value="bm_mutes">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="mutesrecordtable">禁言记录表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="mutesrecordtable" id="mutesrecordtable" value="bm_mutes_records">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="kickstable">踢出表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="kickstable" id="kickstable" value="bm_kicks">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="warningstable">警告表名:</label>
								<div class="controls">
									<input type="text" class="form-control required fixedWidth" name="warningstable" id="warningstable" value="bm_warnings">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn" data-dismiss="modal">关闭</a>
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</div>
	</div>
	<br />
	<br />
	<h3>设置中心 <small>你可以在settings.php设置更多内容</small></h3>
	<form class="form-horizontal settings" action="" method="post">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>项目</th>
					<th>值</th>
				</tr>
			</thead>
			<tbody>
	<?php
	if(!is_writable('settings.php')) {
		echo '
				<tr>
					<td colspan="2">settings.php无法写入</td>
				</tr>';
	} else {
		echo '
				<tr>
					<td>iFrame保护（推荐）</td>
					<td><input type="hidden" name="type" value="mainsettings" /><input type="checkbox" name="iframe"'.((isset($settings['iframe_protection']) && $settings['iframe_protection']) || !isset($settings['iframe_protection']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>使用UTF8编码（与插件配置保持一致，推荐）</td>
					<td><input type="checkbox" name="utf8"'.(isset($settings['utf8']) && $settings['utf8'] ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示页脚</td>
					<td><input type="text" class="form-control" name="footer" value="'.$settings['footer'].'" /></td>
				</tr>
				<tr>
					<td>显示最新封禁</td>
					<td><input type="checkbox" name="latestbans"'.((isset($settings['latest_bans']) && $settings['latest_bans']) || !isset($settings['latest_bans']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示最新禁言</td>
					<td><input type="checkbox" name="latestmutes"'.(isset($settings['latest_mutes']) && $settings['latest_mutes'] ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示最新警告</td>
					<td><input type="checkbox" name="latestwarnings"'.(isset($settings['latest_warnings']) && $settings['latest_warnings'] ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>按钮之前的HTML代码（意义不明）</td>
					<td><input type="text" class="form-control" name="buttons_before" value="'.(isset($settings['submit_buttons_before_html']) ? $settings['submit_buttons_before_html'] : '').'" /></td>
				</tr>
				<tr>
					<td>按钮之后的HTML代码（意义不明）</td>
					<td><input type="text" class="form-control" name="buttons_after" value="'.(isset($settings['submit_buttons_after_html']) ? $settings['submit_buttons_after_html'] : '').'" /></td>
				</tr>';
	} ?>
	
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">
	<?php
	if(!is_writable('settings.php')) {
		echo '<input type="submit" class="btn btn-primary btn-large disabled" disabled="disabled" value="保存" />';
	} else {
		echo '<input type="submit" class="btn btn-primary btn-large" value="保存" />';
	} ?>
			
					</td>
				</tr>
			</tfoot>
		</table>
	</form>
	<br />
	<br />
	<h3>玩家页面设置</h3>
	<form class="form-horizontal settings" action="" method="post">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>项目</th>
					<th>值</th>
				</tr>
			</thead>
			<tbody>
	<?php
	if(!is_writable('settings.php')) {
		echo '
				<tr>
					<td colspan="2">settings.php 无法写入</td>
				</tr>';
	} else {
		echo '
				<tr>
					<td>显示当前的封禁</td>
					<td><input type="hidden" name="type" value="viewplayer" /><input type="checkbox" name="ban"'.((isset($settings['player_current_ban']) && $settings['player_current_ban']) || !isset($settings['player_current_ban']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>当前的封禁页面加入的HTML代码（意义不明）</td>
					<td><input type="input" class="form-control" name="banextra"'.(isset($settings['player_current_ban_extra_html']) ? ' value="'.$settings['player_current_ban_extra_html'].'"' : '').' /></td>
				</tr>
				<tr>
					<td>显示当前的禁言</td>
					<td><input type="checkbox" name="mute"'.((isset($settings['player_current_mute']) && $settings['player_current_mute']) || !isset($settings['player_current_mute']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>当前的禁言页面加入的HTML代码（意义不明）</td>
					<td><input type="input" class="form-control" name="muteextra"'.(isset($settings['player_current_mute_extra_html']) ? ' value="'.$settings['player_current_mute_extra_html'].'"' : '').' /></td>
				</tr>
				<tr>
					<td>显示封禁记录</td>
					<td><input type="checkbox" name="prevbans"'.((isset($settings['player_previous_bans']) && $settings['player_previous_bans']) || !isset($settings['player_previous_bans']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示禁言记录</td>
					<td><input type="checkbox" name="prevmutes"'.((isset($settings['player_previous_mutes']) && $settings['player_previous_mutes']) || !isset($settings['player_previous_mutes']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示警告记录</td>
					<td><input type="checkbox" name="warnings"'.((isset($settings['player_warnings']) && $settings['player_warnings']) || !isset($settings['player_warnings']) ? ' checked="checked"' : '').' /></td>
				</tr>
				<tr>
					<td>显示踢出记录</td>
					<td><input type="checkbox" name="kicks"'.((isset($settings['player_kicks']) && $settings['player_kicks']) || !isset($settings['player_kicks']) ? ' checked="checked"' : '').' /></td>
				</tr>';
	} ?>
	
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">
	<?php
	if(!is_writable('settings.php')) {
		echo '<input type="submit" class="btn btn-primary btn-large disabled" disabled="disabled" value="保存" />';
	} else {
		echo '<input type="submit" class="btn btn-primary btn-large" value="保存" />';
	} ?>
			
					</td>
				</tr>
			</tfoot>
		</table>
	</form>
	<br />
	<br />
	<h3>杂项</h3>
	<a href="index.php?action=deletecache&authid=<?php echo sha1($settings['password']); ?>" class="btn btn-primary">清理缓存</a>
	<?php
}
?>
