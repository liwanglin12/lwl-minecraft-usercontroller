function docReady() {
    function e() {
        var a, b, e, f;
        for (c.length > 0 && (c = c.slice(1)); c.length < d;) a = c.length > 0 ? c[c.length - 1] : 50,
        b = a + 10 * Math.random() - 5,
        0 > b && (b = 0),
        b > 100 && (b = 100),
        c.push(b);
        for (e = [], f = 0; f < c.length; ++f) e.push([f, c[f]]);
        return e
    }
    function i() {
        h.setData([e()]),
        h.draw(),
        setTimeout(i, f)
    }
    var b, c, d, f, g, h;
    $('a[href="#"][data-top!=true]').click(function(a) {
        a.preventDefault()
    }),
    $(".cleditor").cleditor(),
    $(".noty").click(function(a) {
        a.preventDefault();
        var b = $.parseJSON($(this).attr("data-noty-options"));
        noty(b)
    }),
    $("input:checkbox, input:radio, input:file").not('[data-no-uniform="true"],#uniform-is-ajax').uniform(),
    $('[data-rel="chosen"],[rel="chosen"]').chosen(),
    $("#myTab a:first").tab("show"),
    $("#myTab a").click(function(a) {
        a.preventDefault(),
        $(this).tab("show")
    }),
    $(".sortable").sortable({
        revert: !0,
        cancel: ".btn,.box-content,.nav-header",
        update: function() {}
    }),
    $(".slider").slider({
        range: !0,
        values: [10, 65]
    }),
    $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
        placement: "bottom",
        delay: {
            show: 400,
            hide: 200
        }
    }),
    $("textarea.autogrow").autogrow(),
    $('[rel="popover"],[data-rel="popover"]').popover(),
    $(".file-manager").elfinder({
        url: "misc/elfinder-connector/connector.php"
    }).elfinder("instance"),
    $(".iphone-toggle").iphoneStyle(),
    $(".raty").raty({
        score: 4
    }),
    $("#file_upload").uploadify({
        swf: "misc/uploadify.swf",
        uploader: "misc/uploadify.php"
    }),
    $("ul.gallery li").hover(function() {
        $("img", this).fadeToggle(1e3),
        $(this).find(".gallery-controls").remove(),
        $(this).append('<div class="well gallery-controls"><p><a href="#" class="gallery-edit btn"><i class="icon-edit"></i></a> <a href="#" class="gallery-delete btn"><i class="icon-remove"></i></a></p></div>'),
        $(this).find(".gallery-controls").stop().animate({
            "margin-top": "-1"
        },
        400, "easeInQuint")
    },
    function() {
        $("img", this).fadeToggle(1e3),
        $(this).find(".gallery-controls").stop().animate({
            "margin-top": "-30"
        },
        200, "easeInQuint",
        function() {
            $(this).remove()
        })
    }),
    $(".thumbnails").on("click", ".gallery-delete",
    function(a) {
        a.preventDefault(),
        $(this).parents(".thumbnail").fadeOut()
    }),
    $(".thumbnails").on("click", ".gallery-edit",
    function(a) {
        a.preventDefault()
    }),
    $(".thumbnail a").colorbox({
        rel: "thumbnail a",
        transition: "elastic",
        maxWidth: "95%",
        maxHeight: "95%"
    }),
    $("#toggle-fullscreen").button().click(function() {
        var a = $(this),
        b = document.documentElement;
        a.hasClass("active") ? ($("#thumbnails").removeClass("modal-fullscreen"), (document.webkitCancelFullScreen || document.mozCancelFullScreen || $.noop).apply(document)) : ($("#thumbnails").addClass("modal-fullscreen"), b.webkitRequestFullScreen ? b.webkitRequestFullScreen(window.Element.ALLOW_KEYBOARD_INPUT) : b.mozRequestFullScreen && b.mozRequestFullScreen())
    }),
    $(".tour").length && "undefined" == typeof b && (b = new Tour, b.addStep({
        element: ".span10:first",
        placement: "top",
        title: "Custom Tour",
        content: "You can create tour like this. Click Next."
    }), b.addStep({
        element: ".theme-container",
        placement: "left",
        title: "Themes",
        content: "You change your theme from here."
    }), b.addStep({
        element: "ul.main-menu a:first",
        title: "Dashboard",
        content: "This is your dashboard from here you will find highlights."
    }), b.addStep({
        element: "#for-is-ajax",
        title: "Ajax",
        content: "You can change if pages load with Ajax or not."
    }), b.addStep({
        element: ".top-nav a:first",
        placement: "bottom",
        title: "Visit Site",
        content: "Visit your front end from here."
    }), b.restart()),
    $(".datatable").dataTable({
        sDom: "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span12'i><'span12 center'p>>",
        sPaginationType: "bootstrap",
        aaSorting: [[0, "desc"]],
        oLanguage: {
            sLengthMenu: "_MENU_ 条记录每页"
        }
    }),
    $(".btn-close").click(function(a) {
        a.preventDefault(),
        $(this).parent().parent().parent().fadeOut()
    }),
    $(".btn-minimize").click(function(a) {
        a.preventDefault();
        var b = $(this).parent().parent().next(".box-content");
        b.is(":visible") ? $("i", $(this)).removeClass("icon-chevron-up").addClass("icon-chevron-down") : $("i", $(this)).removeClass("icon-chevron-down").addClass("icon-chevron-up"),
        b.slideToggle()
    }),
    $("#external-events div.external-event").each(function() {
        var a = {
            title: $.trim($(this).text())
        };
        $(this).data("eventObject", a),
        $(this).draggable({
            zIndex: 999,
            revert: !0,
            revertDuration: 0
        })
    }),
    $("#calendar").fullCalendar({
        header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
        },
        editable: !0,
        droppable: !0,
        drop: function(a, b) {
            var c = $(this).data("eventObject"),
            d = $.extend({},
            c);
            d.start = a,
            d.allDay = b,
            $("#calendar").fullCalendar("renderEvent", d, !0),
            $("#drop-remove").is(":checked") && $(this).remove()
        }
    }),
    c = [],
    d = 300,
    f = 30,
    $("#updateInterval").val(f).change(function() {
        var a = $(this).val();
        a && !isNaN( + a) && (f = +a, 1 > f && (f = 1), f > 2e3 && (f = 2e3), $(this).val("" + f))
    }),
    $("#realtimechart").length && (g = {
        series: {
            shadowSize: 1
        },
        yaxis: {
            min: 0,
            max: 100
        },
        xaxis: {
            show: !1
        }
    },
    h = $.plot($("#realtimechart"), [e()], g), i())
}
var myDate = new Date;
current_hour = myDate.getHours(),
current_theme = current_hour > 18 || 6 > current_hour ? "cyborg": "classic",
document.getElementById("bs-css").href = "/Public/css/bootstrap-" + current_theme + ".css",
$(document).ready(function() {
    $.browser.msie && ($("#is-ajax").prop("checked", !1), $("#for-is-ajax").hide(), $("#toggle-fullscreen").hide(), $(".login-box").find(".input-large").removeClass("span10")),
    $("ul.main-menu li a").each(function() {
        $($(this))[0].href == String(window.location) && $(this).parent().addClass("active")
    });
    var b = window.History;
    b.getState(),
    $("#log"),
    b.Adapter.bind(window, "statechange",
    function() {
        var a = b.getState();
        $.ajax({
            url: a.url,
            success: function(a) {
                var b, c;
                return $("#content").html($(a).find("#content").html()),
                "location.href='/member/login';" == $(a).html() ? (location.href = "/member/login", void 0) : (b = a.match(/<script id=\"hostkerjs\">([.\s\S]+?)<\/script>/m)[1], window.eval(b), $("#loading").remove(), $("#content").fadeIn(), c = $(a).filter("title").text(), $("title").text(c), docReady(), void 0)
            }
        })
    }),
    $("a.ajax-link").click(function(a) {
        if ($.browser.msie && (a.which = 1), 1 == a.which && !$(this).parent().hasClass("active")) {
            a.preventDefault(),
            $(".btn-navbar").is(":visible") && $(".btn-navbar").click(),
            $("#loading").remove(),
            $("#content").fadeOut().parent().append('<div id="loading" class="center">努力载入中...<div class="center"></div></div>');
            var c = $(this);
            b.pushState(null, null, c.attr("href")),
            $("ul.main-menu li.active").removeClass("active"),
            c.parent("li").addClass("active")
        }
    }),
    $("ul.main-menu li:not(.nav-header)").hover(function() {
        $(this).animate({
            "margin-left": "+=5"
        },
        300)
    },
    function() {
        $(this).animate({
            "margin-left": "-=5"
        },
        300)
    }),
    docReady()
}),
$.fn.dataTableExt.oApi.fnPagingInfo = function(a) {
    return {
        iStart: a._iDisplayStart,
        iEnd: a.fnDisplayEnd(),
        iLength: a._iDisplayLength,
        iTotal: a.fnRecordsTotal(),
        iFilteredTotal: a.fnRecordsDisplay(),
        iPage: Math.ceil(a._iDisplayStart / a._iDisplayLength),
        iTotalPages: Math.ceil(a.fnRecordsDisplay() / a._iDisplayLength)
    }
},
$.extend($.fn.dataTableExt.oPagination, {
    bootstrap: {
        fnInit: function(a, b, c) {
            var f, d = a.oLanguage.oPaginate,
            e = function(b) {
                b.preventDefault(),
                a.oApi._fnPageChange(a, b.data.action) && c(a)
            };
            $(b).addClass("pagination").append('<ul><li class="prev disabled"><a href="#">&larr; ' + d.sPrevious + "</a></li>" + '<li class="next disabled"><a href="#">' + d.sNext + " &rarr; </a></li>" + "</ul>"),
            f = $("a", b),
            $(f[0]).bind("click.DT", {
                action: "previous"
            },
            e),
            $(f[1]).bind("click.DT", {
                action: "next"
            },
            e)
        },
        fnUpdate: function(a, b) {
            var f, g, h, i, j, c = 5,
            d = a.oInstance.fnPagingInfo(),
            e = a.aanFeatures.p,
            k = Math.floor(c / 2);
            for (d.iTotalPages < c ? (i = 1, j = d.iTotalPages) : d.iPage <= k ? (i = 1, j = c) : d.iPage >= d.iTotalPages - k ? (i = d.iTotalPages - c + 1, j = d.iTotalPages) : (i = d.iPage - k + 1, j = i + c - 1), f = 0, iLen = e.length; iLen > f; f++) {
                for ($("li:gt(0)", e[f]).filter(":not(:last)").remove(), g = i; j >= g; g++) h = g == d.iPage + 1 ? 'class="active"': "",
                $("<li " + h + '><a href="#">' + g + "</a></li>").insertBefore($("li:last", e[f])[0]).bind("click",
                function(c) {
                    c.preventDefault(),
                    a._iDisplayStart = (parseInt($("a", this).text(), 10) - 1) * d.iLength,
                    b(a)
                });
                0 === d.iPage ? $("li:first", e[f]).addClass("disabled") : $("li:first", e[f]).removeClass("disabled"),
                d.iPage === d.iTotalPages - 1 || 0 === d.iTotalPages ? $("li:last", e[f]).addClass("disabled") : $("li:last", e[f]).removeClass("disabled")
            }
        }
    }
});