<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!-- 本页面模版来自Hostker -->
<html lang="zh_CN">
<head>
    <meta charset="utf-8">
    <title>新用户注册</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- 样式 -->
    <link id="bs-css" href="/Public/css/bootstrap-classic.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="/Public/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="/Public/css/signup.css" rel="stylesheet">
    <link href="/Public/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
    <link href='/Public/css/fullcalendar.css' rel='stylesheet'>
    <link href='/Public/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
    <link href='/Public/css/chosen.css' rel='stylesheet'>
    <link href='/Public/css/uniform.default.css' rel='stylesheet'>
    <link href='/Public/css/colorbox.css' rel='stylesheet'>
    <link href='/Public/css/jquery.cleditor.css' rel='stylesheet'>
    <link href='/Public/css/jquery.noty.css' rel='stylesheet'>
    <link href='/Public/css/noty_theme_default.css' rel='stylesheet'>
    <link href='/Public/css/elfinder.min.css' rel='stylesheet'>
    <link href='/Public/css/elfinder.theme.css' rel='stylesheet'>
    <link href='/Public/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='/Public/css/opa-icons.css' rel='stylesheet'>
    <link href='/Public/css/uploadify.css' rel='stylesheet'>

    <!-- IE6-8的html5支持 -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="/Public/js/jquery-1.7.2.min.js"></script>
    <script src="/Public/js/jquery.form.js"></script>
    <script src="/Public/js/jquery-ui-1.8.21.custom.min.js"></script>
    <script src="/Public/js/bootstrap-transition.js"></script>
    <script src="/Public/js/bootstrap-alert.js"></script>
    <script src="/Public/js/bootstrap-modal.js"></script>
    <script src="/Public/js/bootstrap-dropdown.js"></script>
    <script src="/Public/js/bootstrap-scrollspy.js"></script>
    <script src="/Public/js/bootstrap-tab.js"></script>
    <script src="/Public/js/bootstrap-tooltip.js"></script>
    <script src="/Public/js/bootstrap-popover.js"></script>
    <script src="/Public/js/bootstrap-button.js"></script>
    <script src="/Public/js/bootstrap-collapse.js"></script>
    <script src="/Public/js/bootstrap-carousel.js"></script>
    <script src="/Public/js/bootstrap-typeahead.js"></script>
    <script src="/Public/js/bootstrap-tour.js"></script>
    <script src="/Public/js/jquery.cookie.js"></script>
    <script src='/Public/js/fullcalendar.min.js'></script>
    <script src='/Public/js/jquery.dataTables.min.js'></script>

    <script src="/Public/js/excanvas.js"></script>
    <script src="/Public/js/jquery.flot.min.js"></script>
    <script src="/Public/js/jquery.flot.pie.min.js"></script>
    <script src="/Public/js/jquery.flot.stack.js"></script>
    <script src="/Public/js/jquery.flot.resize.min.js"></script>

    <script src="/Public/js/jquery.chosen.min.js"></script>
    <script src="/Public/js/jquery.uniform.min.js"></script>
    <script src="/Public/js/jquery.colorbox.min.js"></script>
    <script src="/Public/js/jquery.cleditor.min.js"></script>
    <script src="/Public/js/jquery.noty.js"></script>
    <script src="/Public/js/jquery.elfinder.min.js"></script>
    <script src="/Public/js/jquery.raty.min.js"></script>
    <script src="/Public/js/jquery.iphone.toggle.js"></script>
    <script src="/Public/js/jquery.autogrow-textarea.js"></script>
    <script src="/Public/js/jquery.uploadify-3.1.min.js"></script>
    <script src="/Public/js/jquery.history.js"></script>
    <script src="/Public/js/signup.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        var options = {
            success: dologin,
            error: function(ob,errStr) {
                showmsg('注册失败，网络好像出岔子了QAQ');
                return false;
            }
        }

        function dologin(statdata,statcode)
        {
            if(statdata=='0')
            {
                showmsg('~\(≧▽≦)/~~~~恭喜，提交成功！请到您的邮箱中收取验证码进行激活！');
                setTimeout(function(){location.href='/';},15000) 
            }else{
                showmsg(statdata);
            }
            return false;
        }

        function showmsg(msgs)
        {
            $('#msgs').text(msgs);
        }

        $('#signup').submit(function(e){
            showmsg('正在注册请稍候喔030');
            $(this).ajaxSubmit(options);
            return false;
        });
    });

    function docheckuser()
    {
        if($("input[name='checkuser']:checked").val()=='on')
            $('#limituser').slideDown();
        else
            $('#limituser').slideUp();
    }
    </script>
</head>

<body>
        <div class="container-fluid">
        <div class="row-fluid">
        
            <div class="row-fluid">
                <div class="span12 center login-header">
                    <h2>欢迎加入哲学服务器</h2>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="well span5 center login-box">
                    <div class="alert alert-info" id="msgs">
                        请勿一人多号
                    </div>
                    <form class="form-horizontal" action="/Home/User/signup" id="signup" method="post">
                        <fieldset>
                            用户名/游戏ID<br />
                            <div class="input-prepend" title="姓名" data-rel="tooltip">
                                <span class="add-on"><i class="loginicon-user"></i></span><input autofocus class="input-large span10" name="name" id="fullname" type="text" required />
                            </div>
                            <div class="clearfix"></div>

                            邮箱<br />
                            <div class="input-prepend" title="邮箱很重要的说=w=" data-rel="tooltip">
                                <span class="add-on"><i class="loginicon-signal"></i></span><input autofocus class="input-large span10" name="email" id="email" type="email" required />
                            </div>
                            <div class="clearfix"></div>

                            密码<br />
                            <div class="input-prepend" title="密码什么的" data-rel="tooltip">
                                <span class="add-on"><i class="loginicon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="" required />
                            </div>
                            <div class="clearfix"></div>

                            重复密码<br />
                            <div class="input-prepend" title="重复密码认真填啦" data-rel="tooltip">
                                <span class="add-on"><i class="loginicon-lock"></i></span><input class="input-large span10" name="password2" id="password2" type="password" value="" required />
                            </div>
                             <div class="clearfix"></div>

                            QQ<br />
                            <div class="input-prepend" title="马上就好了快填!" data-rel="tooltip">
              <span class="add-on"><i class="loginicon-user"></i></span><input class="input-large span10" name="qq" id="qq" type="text" value="" required " />
                            </div>
                            <div class="clearfix"></div-->

                            <label class="checkbox inline">
                                    <input type="checkbox" id="checkuser" name="checkuser" onclick="javascript:docheckuser();">我同意以下规定
                            </label>
                            <br /><a href="#userTOS" data-toggle="modal">哲学服务器用户注册协议</a>
                            <br /><span style="font-size:18px;color:red;">哲学服务器仅向拥有基础MC知识的用户提供服务</style>
                            
                            <p class="center span5">
                            <button type="submit" class="btn btn-primary" id="limituser" style="display:none;">注册</button>
                            </p>
                            <div class="clearfix"></div>
                        </fieldset>
                    </form>
                </div><!--/span-->
            </div><!--/row-->
                </div><!--/fluid-row-->

    <div class="modal hide fade" id="userTOS">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>哲学服务器用户注册协议</h3>
        </div>
        <div class="modal-body">
            <p>在以下条款中，“用户”是指向哲学服务器申请注册成为哲学服务器用户中心/游戏会员的个人或集体单位，“本站”是指mc.lwl12.com以及其它由哲学服务器运营的下属服务。</p><p>用户同意此在线注册条款之效力如同用户亲自签字、盖章的书面条款一样，对用户具有法律约束力。</p><p>用户进一步同意，用户进入本站会员注册程序即意味着用户同意了本注册条款的所有内容且只有用户完全同意所有服务条款并完成注册程序，才能成为本站的正式用户。本注册条款自用户注册成功之日起在用户与本站之间产生法律效力。</p><p><b>用户的权利和义务</b></p><p>1.用户承诺并保证自己是具有完全民事行为能力和完全民事权利能力的自然人、法人、实体和组织，对未满18周岁的用户由其监护人作为本协议的“用户”。用户在此保证所填写的用户信息是真实、准确、完整、及时的，并且没有任何引人误解或者虚假的陈述，且保证本站可以通过用户所填写的联系方式与用户取得联系。</p><p>2.用户将对电子邮件地址、QQ号码和密码安全负全部责任，并且用户对以其用户名进行的所有活动和事件负全责。用户有权根据本站规定的程序修改电子邮件地址、QQ号码和密码。</p><p>3.用户将自己的电子邮件地址、QQ号码、密码擅自转让或授权他人使用，由用户承担由此造成的一切后果。</p><p>4.用户凭Email或已验证的QQ号登录客户中心/游戏可享受本站提供的服务，若发现任何非法使用用户帐号或存在安全漏洞的情况，请立即通告本站。</p><p>5.用户账号不得交给他人，一经发现我们有权冻结账号名下全部业务包括VIP。</p><p>6.用户需承诺遵守全球各地相关法律法规规定，包括但不限于中华人民共和国以及其它我们今后所可能增加的节点所在国家地区的相关法律法规规定。</p><p><b>本站的权利和义务</b></p><p>1.本站为用户提供电子邮件地址验证服务和修改电子邮件地址时向现有电子邮件地址发送验证代码的安全保障。</p><p>2.本站为用户提供QQ号码短信验证服务和修改QQ号码需要原QQ短信确认的安全保障。</p><p>3.本站有权在必要时修改本页面许可协议分类下的条款，服务条款一旦发生变动，将会在重要页面上提示修改内容，并立即Email通知用户。如果不同意所改动的内容，用户可以主动取消获得的网络服务。如果用户继续享用本站网络服务，则视为接受服务条款的变动。本站修改其服务体系和价格Email通知用户3天后视为接受服务条款的变动。</p><p>4.本站承诺对用户资料采取对外保密措施，不向第三方披露用户资料，不授权第三方使用用户资料，不明文记录用户的客户中心/游戏登录密码。</p><p>5.依照法律法规，行政、司法等有权部门要求本站提供协助时，我们会在尽可能保障用户权益的情况下依法配合有关部门提供相关数据。</p><p>6.本站保留在用户违反国家、地方法律法规规定或违反本站许可协议的情况下终止为用户提供服务并终止用户账号的权利，并且在任何情况下，本站对任何间接、偶然、特殊导致的损失不负责任。</p><p><b>不可抗力</b><br />
因不可抗力或者其他意外事件，使得本条款履行不可能、不必要或者无意义的，遭受不可抗力、意外事件的一方不承担责任。不可抗力定义以中华人民共和国法律定性为主，在本协议中，用户同意鉴于互联网的特殊性，黑客攻击、互联网连通中断、系统故障或者政策/有关部门管制等属于不可抗力，由此给用户或者第三方造成的损失不应由本站承担。</p><p><b>用户在此再次保证已经完全阅读并理解了上述条款并自愿正式进入本站会员在线注册程序，接受上述所有条款的约束。</b></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">关闭</a>
        </div>
    </div>

    </div><!--/.fluid-container-->

    
</body>
</html>