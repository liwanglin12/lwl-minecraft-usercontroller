<?php
/**
 * @author 小影
 * @link http://c7sky.com/wordpress-theme-minty.html
 * @copyright [小影志](http://c7sky.com/)
 */
?>

		<?php if (get_option('minty_breadcrumb') == 'bottom' && (get_option('minty_breadcrumb_nohome') == 1 ? !is_home() : true)) minty_breadcrumb(); ?>
		
		<?php if( is_home() ) do_action('minty_container_end'); ?>
	</div>

	<?php
	if ( get_option('minty_mobile_menu_type', 'select' ) == 'div' ) {
		wp_nav_menu( array(
			'theme_location'  => 'mobile',
			'container'       => false,
			'menu_class'      => 'mobile-menu',
			'depth'           => 2
		) );
	}
	?>

	<?php get_template_part('ad', 'footer'); ?>

	<footer id="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
<!-- 		<nav class="links">
	<?php
	// $footernav = strip_tags( wp_nav_menu( array(
	// 		'theme_location'  => 'footer',
	// 		'container'       => false,
	// 		'depth'           => 1,
	// 		'echo'            => false
	// 	) ), '<a>' );
	// $footernav = preg_replace( '/\n<a/', ' ' . get_option('minty_footer_nav_separator') . ' <a', $footernav);
	// echo $footernav;
	?>
</nav> -->
		<div class="copyright">
			&copy; <?php $startYear = get_option('minty_footer_year'); if (!empty($startYear)) echo $startYear .'-'; echo date('Y'); ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a> &middot; Powered by <a href="http://wordpress.org/" target="_blank" rel="generator">WordPress</a> &middot; Theme by <a href="http://c7sky.com/" target="_blank">小影</a><?php echo stripslashes(get_option('minty_footer_code'));
			if( get_option('minty_stat_noadmin') == true ? !current_user_can('edit_dashboard') : true ) {
				echo get_option('minty_stat_hidden') == true
					? '<div class="stat">' . stripslashes(get_option('minty_stat_code')) . '</div>'
					: stripslashes(get_option('minty_stat_code'));
			}; ?>
		</div>
		liwanglin12's <a href="http://blog.lwl12.com" target="_blank">博客</a> | <a href="http://code.lwl12.com" target="_blank">代码</a> | <a href="http://api.lwl12.com" target="_blank">API</a> | <a href="http://tbsign.lwl12.com/" target="_blank">云签</a> | <script type="text/javascript" src="https://lwlapi.sinaapp.com/hitokoto/?encode=js&amp;charset=utf-8"></script> <script>lwlhitokoto()</script>
		<a id="rocket" href="#top" title="返回顶部"><i></i></a>
	</footer>

<?php
if ( !is_user_logged_in() && get_option('minty_header_userinfo') == 1 ) {
	echo '<table id="minty_login"><tbody><tr><td><div id="minty_loginbox"><h3 class="login-title">登录</h3>';
	wp_login_form();
	echo '<p class="extra-links">';
	wp_register('', ' | ');
	echo '<a href="' . wp_lostpassword_url() . '">忘记密码</a>';
	echo '</p></div></td></tr></tbody></table>';
}
?>
<?php wp_footer(); ?>

</body>
</html>