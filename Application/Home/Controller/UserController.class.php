<?php
namespace Home\Controller;
use Think\Controller;
date_default_timezone_set('Asia/Shanghai');
class UserController extends Controller
{
    public function login()
    {
        if (!IS_POST) {
            header('HTTP/1.1 405 Method Not Allowed');
            die();
        } else if (empty($_POST["email"]) || empty($_POST["pw"]) || empty($_POST["ckey"])) {
            Header("http/1.0 400 Bad Request");
            die();
        } else if (md5(md5($_POST["email"] . $_POST["pw"]) . substr($_SERVER["REQUEST_TIME"], 0, 9) . "V:GLU_Q!") != $_POST["ckey"]) {
            $msg = array(
                'code' => '1',
                'msg' => 'ckey validation fails',
                'display' => '非法登录'
            );
            die(json_encode($msg));
        }
        $db     = M('mc_user');
        $ban    = M('bm_bans_14');
        $pw     = base64_decode($_POST["pw"]);
        $result = $db->where("email='%s'", $_POST["email"])->select();
        $result = $result[0];
        if ($result == NULL) {
            $msg = array(
                'code' => '1',
                'msg' => 'Not Found User'
            );
            die(json_encode($msg));
        }
        if ($result["verify"] != 'true') {
            $msg = array(
                'code' => '1',
                'msg' => 'Unverified Account'
            );
            die(json_encode($msg));
        }
        $banres = $ban->where("banned='%s'", $result["name"])->select();
        $banres = $banres[0];
        if ($banres["banned"] == $result["name"]) {
            $msg = array(
                'code' => '1',
                'msg' => 'Account be banned',
                'name' => $result["name"],
                'reason' => $banres["ban_reason"]
            );
            die(json_encode($msg));
        }
        $authacc = array(
            'E_mpty',
            'liwanglin12',
            'zariba',
            'ruitz',
            'rtz',
            'U2FsdGVkX1',
            'Fanul',
            'Y_lfrettub',
            'king_lxy',
            'Ken_HY',
            'Zeta13',
            'SDyangzai',
            'Seation'
        );
        
        if (!in_array($result["name"], $authacc)) {
            $msg = array(
                'code' => '1',
                'msg' => 'Unauthorized Account',
                'display' => '您的账号未被授权进入14周目内测'
            );
            die(json_encode($msg));
        }
        if (password_verify($pw, $result["pw"])) {
            $ip = isset($_SERVER["HTTP_CDN_REAL_IP"]) ? $_SERVER["HTTP_CDN_REAL_IP"] : $_SERVER["REMOTE_ADDR"];
            if ($result["logintime"] + 300 >= time()) {
                $msg           = array(
                    'code' => '0',
                    'msg' => 'Login successful',
                    'online' => $result["online"],
                    'name' => $result["name"],
                    'exinfo' => '-Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true',
                    'title' => 'Ph Craft - Based on Minecraft 1.7.10'
                );
                $db->loginip   = $ip;
                $db->logintime = time();
                $db->where("email='%s'", $_POST["email"])->save();
                exit(json_encode($msg));
            }
            $db->loginip   = $ip;
            $db->logintime = time();
            $db->where("email='%s'", $_POST["email"])->save();
            $msg    = array(
                'code' => '0',
                'msg' => 'Login successful',
                'online' => $result["online"],
                'name' => $result["name"],
                'exinfo' => '-Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true',
                'title' => 'Ph Craft - Based on Minecraft 1.7.10'
            );
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            socket_connect($socket, "10.163.180.96", 23333);
            $in  = $result["qq"] . "|" . $result["name"] . "|" . $ip;
            $out = '';
            socket_write($socket, $in, strlen($in));
            while ($out = socket_read($socket, 1)) {
                $result = $out;
            }
            socket_close($socket);
            exit(json_encode($msg));
        } else {
            $msg = array(
                'code' => '1',
                'msg' => 'Password Error'
            );
            die(json_encode($msg));
        }
    }
    public function logout()
    {
        if (!IS_POST) {
            header('HTTP/1.1 405 Method Not Allowed');
            die();
        } else if (empty($_POST["email"]) || empty($_POST["pw"]) || empty($_POST["ckey"])) {
            Header("http/1.0 400 Bad Request");
            die();
        } else if (md5(md5($_POST["email"] . $_POST["pw"]) . substr($_SERVER["REQUEST_TIME"], 0, 9) . "V:GLU_Q!") != $_POST["ckey"]) {
            $msg = array(
                'code' => '1',
                'msg' => 'ckey validation fails'
            );
            die(json_encode($msg));
        }
        $db     = M('mc_user');
        $pw     = base64_decode($_POST["pw"]);
        $result = $db->where("email='%s'", $_POST["email"])->select();
        $result = $result[0];
        if ($result == NULL) {
            $msg = array(
                'code' => '1',
                'msg' => 'Not Found User'
            );
            die(json_encode($msg));
        }
        if ($result["verify"] != 'true') {
            $msg = array(
                'code' => '1',
                'msg' => 'Unverified Account'
            );
            die(json_encode($msg));
        }
        if (password_verify($pw, $result["pw"])) {
            $db->loginip   = isset($_SERVER["HTTP_CDN_REAL_IP"]) ? $_SERVER["HTTP_CDN_REAL_IP"] : $_SERVER["REMOTE_ADDR"];
            $db->logintime = '0';
            $db->where("email='%s'", $_POST["email"])->save();
            $msg = array(
                'code' => '0',
                'msg' => 'Logout successful'
            );
            exit(json_encode($msg));
        } else {
            $msg = array(
                'code' => '1',
                'msg' => 'Password Error'
            );
            die(json_encode($msg));
        }
    }
    
    public function check()
    {
        $db = M('mc_user');
        if (!empty($_GET["name"]) && !empty($_GET["ip"])) {
            $result = $db->where("name='%s'", $_GET["name"])->select();
            $result = $result[0];
            if ($result == NULL) {
                exit('1');
            } else if ($result["logintime"] + 300 < time()) {
                exit('3');
                //} else if ($result["loginip"] != $_GET["ip"]) {
                //  exit('2');
            } else {
                exit('0');
            }
        } else {
            exit();
        }
    }
    public function signup()
    {
        if (!IS_AJAX) {
            $this->display();
            die();
        }
        if (empty($_POST["name"]) || empty($_POST["email"]) || empty($_POST["password"]) || empty($_POST["password2"]) || empty($_POST["qq"])) {
            Header("http/1.0 400 Bad Request");
            die();
        }
        import('ORG.Util.Session');
        if (session('?signup')) {
            die('您已注册账号' . session('signup') . ',禁止重复注册！');
        }
        if ($_POST["checkuser"] != "on") {
            die('您需要同意用户协议才能注册！');
        }
        $black_list = array(
            "LWL12",
            "lwl12",
            "liwanglin",
            "WoShiLWL12",
            "LWL21",
            "fucklwl",
            "Fucklwl"
        );
        if (in_array($_POST["name"], $black_list)) {
            die('此ID已被系统保护，无法注册');
        }
        $db     = M('mc_user');
        $result = $db->where("name='%s'", $_POST["name"])->select();
        if ($result != NULL) {
            die('=w=这个用户名看起来已经被使用了呢');
        }
        $result = $db->where("email='%s'", $_POST["email"])->select();
        if ($result != NULL) {
            die('=w=这个邮箱看起来已经被使用了呢');
        }
        $result = $db->where("qq='%s'", $_POST["qq"])->select();
        if ($result != NULL) {
            die('=w=这个QQ看起来已经被使用了呢');
        }
        if ($_POST["password"] != $_POST["password2"]) {
            die('( ╯#-_-)╯ ┻━┻两次密码输入不一致啊');
        }
        if (!preg_match('/^[_\-@#.!0-9a-zA-Z]{6,30}$/i', $_POST["password"])) {
            die('_(:з」∠)_密码格式：_ - @ # . ! ，0-9，A-Z，a-z组成的6-30位字符串，不能填写中文~而你填写的密码好像并不符合这个格式');
        }
        if (!preg_match('/^[_0-9a-zA-Z]{3,30}$/i', $_POST["name"])) {
            die('_(:з」∠)_用户名格式：下划线，数字，大小写字母组成的3-30位字符串，不能填写中文或特殊字符~而你填写的用户名好像并不符合这个格式');
        }
        $postdata = '{"agent": { "name": "Minecraft", "version": 1 }, "username": "' . $_POST['email'] . '",  "password": "' . $_POST['password'] . '"}';
        $result   = json_decode(spost('https://authserver.mojang.com/authenticate', $postdata, array(
            'Content-Type: application/json'
        )), true);
        $result   = $result["availableProfiles"][0];
        if (isset($result["name"])) {
            if ($result["name"] != $_POST["name"]) {
                die('( ⊙ o ⊙ )，我们进行正版登录测试后发现该邮箱与密码拥有一个正版ID:' . $result["name"] . ',请您使用该ID注册哦');
            } else {
                $udata['online'] = 'true';
            }
        } else {
            if (file_get_contents('https://minecraft.net/haspaid.jsp?user=' . $_POST['name']) == 'true') {
                die('( ⊙ o ⊙ )，你使用的ID是一个正版ID，填写匹配的邮箱和密码才能注册哦!');
            } else {
                $udata['online'] = 'false';
            }
        }
        $arr = array(
            1 => "0123456789",
            2 => "abcdefghijklmnopqrstuvwxyz",
            3 => "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        );
        array_pop($arr);
        $string          = implode("", $arr);
        $count           = strlen($string) - 1;
        $udata['verify'] = '';
        for ($i = 0; $i < 16; $i++) {
            $udata['verify'] .= $string[rand(0, $count)];
        }
        $content = '<div style="background-color:white;border-top:2px solid #12ADDB;box-shadow:0 1px 3px #AAAAAA;line-height:180%;padding:0 15px 12px;width:500px;margin:50px auto;color:#555555;font-family:Century Gothic,Trebuchet MS,Hiragino Sans GB,微软雅黑,Microsoft Yahei,Tahoma,Helvetica,Arial,SimSun,sans-serif;font-size:12px;"> <h2 style="border-bottom:1px solid #DDD;font-size:14px;font-weight:normal;padding:13px 0 10px 8px;"> <span style="color: #12ADDB;font-weight: bold;">&gt; </span> <a style="text-decoration:none;color: #12ADDB;" href="https://mc.lwl12.com/" se_prerender_url="complete"> Ph Craft </a>注册验证</h2> <div style="padding:0 12px 0 12px;margin-top:18px"> <p>' . $_POST["name"] . ' ，欢迎加入<strong>哲学服务器</strong>！</p><p>请私聊发送以下代码到群内机器人以激活您的账号</p> <p style="background-color: #f5f5f5;border: 0px solid#DDD;padding: 10px 15px;margin:18px 0">MCHECK' . $udata['verify'] . '</p><p>发件时间： ' . date("Y-m-d H:i:s") . '</p><p>如您未在PhC申请注册，请忽略此邮件</p></div> </div>';
        if (!sendMail($_POST["email"], $_POST["name"], "[Ph Craft]请验证您的邮箱与QQ", $content)) {
            sleep(1);
            if (!sendMail($_POST["email"], $_POST["name"], "[Ph Craft]请验证您的邮箱与QQ", $content)) {
                sleep(1);
                if (!sendMail($_POST["email"], $_POST["name"], "[Ph Craft]请验证您的邮箱与QQ", $content)) {
                    die('验证邮件发送失败,请联系在线OP_(:з」∠)_');
                }
            }
        }
        $udata['name']      = $_POST["name"];
        $udata['pw']        = password_hash($_POST["password"], PASSWORD_BCRYPT);
        $udata['email']     = $_POST["email"];
        $udata['qq']        = $_POST["qq"];
        $udata['regip']     = isset($_SERVER["HTTP_CDN_REAL_IP"]) ? $_SERVER["HTTP_CDN_REAL_IP"] : $_SERVER["REMOTE_ADDR"];
        $udata['loginip']   = isset($_SERVER["HTTP_CDN_REAL_IP"]) ? $_SERVER["HTTP_CDN_REAL_IP"] : $_SERVER["REMOTE_ADDR"];
        $udata['logintime'] = 0;
        $db->add($udata);
        session(array(
            'signup' => $udata['name'],
            'expire' => 3155692600
        ));
        session('signup', $udata['name']);
        exit('0');
    }
    public function qqverify()
    {
        if (empty($_GET['key']) || empty($_GET['code']) || empty($_GET['qq'])) {
            Header("http/1.0 400 Bad Request");
        }
        if ($_GET['key'] != 'ov.-wamH') {
            Header("http/1.0 400 Bad Request");
        }
        $db     = M('mc_user');
        $result = $db->where("verify='%s'", $_GET["code"])->select();
        $result = $result[0];
        if ($result == NULL) {
            die('未找到该校验码!');
        }
        if ($result["qq"] != $_GET['qq']) {
            die('您在注册页面填写的QQ号码是' . $result["qq"] . ',与当前QQ不符！');
        }
        $data['verify'] = 'true';
        $db->where("verify='%s'", $_GET["code"])->save($data); // 根据条件更新记录
        exit('校验成功！您现在可以使用账号' . $result["name"] . '登入服务器!');
    }
    
}
