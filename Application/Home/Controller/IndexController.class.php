<?php

namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller
{
    public function index()
    {
        dump('hello world');
    }
    public function onlist()
    {
        $db     = M('mc_user');
        $result = $db->select();
        foreach ($result as $user) {
            if ($user["logintime"] + 300 >= time()) {
                echo $user["name"] . "|";
            }
        }
    }
    public function password()
    {
        if (!preg_match('/^[_\-@#.!0-9a-zA-Z]{6,30}$/i', $_GET["pw"])) {
            die('_(:з」∠)_密码格式：_ - @ # . ! ，0-9，A-Z，a-z组成的6-30位字符串，不能填写中文~而你填写的密码好像并不符合这个格式');
        }
        echo(password_hash($_GET["pw"], PASSWORD_BCRYPT));
    }
}
