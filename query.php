<?php
date_default_timezone_set('Asia/Shanghai');
if (empty($_POST["name"])) {
?>
  <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Ph Craft登录历史查询</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <style>
        body{
            font-family: "microsoft YaHei";
        }
        h2{
            text-align: center;
        }
        .page-footer{
            padding: 30px 0px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <section>
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="page-header">
                        <h2>Ph Craft登录历史查询</h2>
                    </div>

                    <form id="" class="form-horizontal bv-form">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">游戏ID</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control username1" name="username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">管理口令</label>
                            <div class="col-lg-5">
                                <input class="form-control pw" name="pw" type="password">
                                输入管理口令，查看完整信息（非必填）
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <button type="button" class="btn btn-primary" name="" value="查询">查询</button>
                            </div>
                        </div>
                          <div id="notice-i" class="alert alert-info" role="alert">
                  <b>欢迎使用Ph Craft登录历史查询系统</b>
              </div>
              <div id="notice" class="alert alert-success" role="alert" display:"block">
                  
              </div>
              <div id="notice-e" class="alert alert-danger" role="alert" display:"block">
                  查询失败！
              </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="page-footer">
        如有疑问请<a href="/">前往论坛</a>
    </div>
<script src="https://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
  $('#notice-e').addClass('hide');
  $('#notice').addClass('hide');
  $(function(){
    function text(){
      $(".btn").on('click',function(){
        if($(".username1").val()==""){
          alert("ID不能为空");
          return false;
        }
        $.ajax({
            url: location.href,
            type: 'POST',
            data: {name:$(".username1").val(),pw:$(".pw").val()},
            success: function(resp){
            $('#notice-i').addClass('hide');
            $('#notice-e').addClass('hide');
            $('#notice').removeClass('hide');
              document.getElementById('notice').innerHTML = resp;
            },
            error: function(){
                $('#notice-i').addClass('hide');
              $('#notice').addClass('hide');
              $('#notice-e').removeClass('hide');
            }
        });
      });      
    }
     text();
  });
</script>
</body>
</html>
<?php
    exit();
}
$con = mysql_connect("phcraftdb.mysql.rds.aliyuncs.com:3308", "log", "LWL_log");
if (!$con) {
    die('Could not connect: ' . mysql_error());
}
mysql_select_db("log", $con);

$result = mysql_query("SELECT `name`,`ip`,`time`,`action` FROM loginlog where `name` = '" . $_POST["name"] . "' order by id desc");
if (!$result) {
        die('Error: ' . mysql_error());
    }
if (mysql_num_rows($result) == 0) {
    exit("无法查询到您的登录历史，请确认您的昵称正确！");
}
echo '<table class="table table-striped table-bordered">
<tr>
<th>ID</th>
<th>IP</th>
<th>时间</th>
<th>操作</th>
</tr>';

while ($row = mysql_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['name'] . "</td>";
    if ($row['action'] == "logout") {
      echo "<td>NULL</td>";
    } else if ($_POST["pw"] == "PHC-admin-query") {
      echo "<td>" . $row['ip'] . "</td>";
    } else {
      echo "<td>" . ip2unknown($row['ip']) . "</td>";
    }
    echo "<td>" . date("Y-m-d H:i:s", $row['time']) . "</td>";
    echo "<td>" . action2text($row['action']) . "</td>";
    echo "</tr>";
}
echo "</table>";

function ip2unknown($ip)
{
    $add = explode('.', $ip, 4);
    return $add[0] . '.' . $add[1] . '.*.*';
}

function action2text($action)
{
    if ($action == "login") {
        return "登录";
    } else {
        return "退出";
    }
    return "Unknown";
}

mysql_close($con);