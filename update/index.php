<?php
set_time_limit(0);
header("content-type:application/json");

if (file_exists("./" . date("Y-m-d") . "-update.json")) {
    $fp = fopen("./" . date("Y-m-d") . "-update.json", 'r');
    while (!feof($fp)) {
        $buffer = fgets($fp, 4096);
        echo $buffer;
    }
    fclose($fp);
    exit();
}

$files = array();
$num   = 1;
recurDir('./file', 1);
function recurDir($pathName, $i)
{
    global $files, $num;
    $dir = $pathName;
    $dh  = opendir($dir);
    while (false !== ($filename = readdir($dh))) {
        if ($filename != "." && $filename != "..") {
            if (is_dir($pathName . '/' . $filename)) {
                recurDir($pathName . '/' . $filename, $i + 1);
                continue;
            }
            if ($filename == 'PHCL.exe') {
                $files["file0"]["name"] = $filename;
                $files["file0"]["md5"]  = md5_file($pathName . '/' . $filename);
                $files["file0"]["path"] = substr($pathName, 7, strlen($pathName) - 7);
            } else {
                $files["file" . $num]["name"] = $filename;
                $files["file" . $num]["md5"]  = md5_file($pathName . '/' . $filename);
                $files["file" . $num]["path"] = substr($pathName, 7, strlen($pathName) - 7);
                $num++;
            }
        }
    }
    if ($i == 1) {
        $files["num"] = $num - 1;
        $json         = json_encode($files);
        echo $json;
        file_put_contents("./" . date("Y-m-d") . "-update.json", $json);
    }
}
?>